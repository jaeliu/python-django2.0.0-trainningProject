__author__='fanxn19000'
__date__='2018/09/25'

from django.db import models

class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    age = models.IntegerField(null=True)
    empl_number= models.IntegerField(null=True)
    realm_account = models.CharField(max_length=100)
    department= models.CharField(max_length=100)
    test_group= models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    sex = models.CharField(max_length=1)

    def __str__(self):
        return '%d %s %s %d %s %s %s %s %s %s' % (self.user_id, self.name, self.password, self.age, self.empl_number,
                               self.realm_account,self.department,self.test_group,self.email,self.sex)


class UserToken(models.Model):
    id = models.AutoField(primary_key=True)
    realm_account = models.CharField(max_length=30)
    user_token = models.CharField(max_length=300)
    login_day = models.DateTimeField()
    objects = models.Manager()

    def __str__(self):
        return '%d %s %s %s' % (self.id,self.realm_account,self.user_token,self.login_day)