from django.conf.urls import url
from . import views

urlpatterns = [
    # 登录
    url(r'^login$', views.login, name='login'),
    # 查询个人信息
    url(r'^qry$', views.userQuery, name='userQuery'),
    # 查询所有用户信息
    url(r'^qryAll$', views.userQueryAll, name='userQueryAll'),
    # 创建用户
    url(r'^create$', views.userCreate, name='userCreate'),
    # 更新用户信息
    url(r'^update$', views.userUpdate, name='userUpdate'),
    # 用户删除
    url(r'^delete$', views.userDelete, name='userDelete'),
    # 用户登出
    url(r'^loginOut$', views.loginOut, name='loginOut'),
    # 查询部门列表
    url(r'^qryDepartments$', views.qryAllDepartment, name='getAllDepartment'),
]
