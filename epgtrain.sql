/*
Navicat MySQL Data Transfer

Source Server         : localhost_本地数据库
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : epgtrain

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2018-11-12 09:57:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can add user', '2', 'add_user');
INSERT INTO `auth_permission` VALUES ('5', 'Can change user', '2', 'change_user');
INSERT INTO `auth_permission` VALUES ('6', 'Can delete user', '2', 'delete_user');
INSERT INTO `auth_permission` VALUES ('7', 'Can add permission', '3', 'add_permission');
INSERT INTO `auth_permission` VALUES ('8', 'Can change permission', '3', 'change_permission');
INSERT INTO `auth_permission` VALUES ('9', 'Can delete permission', '3', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('10', 'Can add group', '4', 'add_group');
INSERT INTO `auth_permission` VALUES ('11', 'Can change group', '4', 'change_group');
INSERT INTO `auth_permission` VALUES ('12', 'Can delete group', '4', 'delete_group');
INSERT INTO `auth_permission` VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO `auth_permission` VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO `auth_permission` VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO `auth_permission` VALUES ('19', 'Can add user', '7', 'add_user');
INSERT INTO `auth_permission` VALUES ('20', 'Can change user', '7', 'change_user');
INSERT INTO `auth_permission` VALUES ('21', 'Can delete user', '7', 'delete_user');
INSERT INTO `auth_permission` VALUES ('22', 'Can add user', '8', 'add_user');
INSERT INTO `auth_permission` VALUES ('23', 'Can change user', '8', 'change_user');
INSERT INTO `auth_permission` VALUES ('24', 'Can delete user', '8', 'delete_user');
INSERT INTO `auth_permission` VALUES ('25', 'Can add lesson', '9', 'add_lesson');
INSERT INTO `auth_permission` VALUES ('26', 'Can change lesson', '9', 'change_lesson');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete lesson', '9', 'delete_lesson');
INSERT INTO `auth_permission` VALUES ('28', 'Can add user token', '10', 'add_usertoken');
INSERT INTO `auth_permission` VALUES ('29', 'Can change user token', '10', 'change_usertoken');
INSERT INTO `auth_permission` VALUES ('30', 'Can delete user token', '10', 'delete_usertoken');
INSERT INTO `auth_permission` VALUES ('31', 'Can add django job', '11', 'add_djangojob');
INSERT INTO `auth_permission` VALUES ('32', 'Can change django job', '11', 'change_djangojob');
INSERT INTO `auth_permission` VALUES ('33', 'Can delete django job', '11', 'delete_djangojob');
INSERT INTO `auth_permission` VALUES ('34', 'Can add django job execution', '12', 'add_djangojobexecution');
INSERT INTO `auth_permission` VALUES ('35', 'Can change django job execution', '12', 'change_djangojobexecution');
INSERT INTO `auth_permission` VALUES ('36', 'Can delete django job execution', '12', 'delete_djangojobexecution');
INSERT INTO `auth_permission` VALUES ('37', 'Can add signup', '13', 'add_signup');
INSERT INTO `auth_permission` VALUES ('38', 'Can change signup', '13', 'change_signup');
INSERT INTO `auth_permission` VALUES ('39', 'Can delete signup', '13', 'delete_signup');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('1', '1', '2018-09-10 16:10:57', '1', '1', '1', '1', '1', '1', '1', '2018-09-06 16:11:07');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for django_apscheduler_djangojob
-- ----------------------------
DROP TABLE IF EXISTS `django_apscheduler_djangojob`;
CREATE TABLE `django_apscheduler_djangojob` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `next_run_time` datetime DEFAULT NULL,
  `job_state` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `django_apscheduler_djangojob_next_run_time_2f022619` (`next_run_time`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_apscheduler_djangojob
-- ----------------------------
INSERT INTO `django_apscheduler_djangojob` VALUES ('1', 'task_time', '2018-10-30 05:55:05', 0x800495DE030000000000007D94288C0776657273696F6E944B018C026964948C097461736B5F74696D65948C0466756E63948C18747261696E557365722E76696577733A746573745F6A6F62948C0774726967676572948C1961707363686564756C65722E74726967676572732E63726F6E948C0B43726F6E547269676765729493942981947D942868014B028C0874696D657A6F6E65948C047079747A948C025F70949394288C0D417369612F5368616E67686169944DE8714B008C034C4D5494749452948C0A73746172745F64617465944E8C08656E645F64617465944E8C066669656C6473945D94288C2061707363686564756C65722E74726967676572732E63726F6E2E6669656C6473948C09426173654669656C649493942981947D94288C046E616D65948C0479656172948C0A69735F64656661756C7494888C0B65787072657373696F6E73945D948C2561707363686564756C65722E74726967676572732E63726F6E2E65787072657373696F6E73948C0D416C6C45787072657373696F6E9493942981947D948C0473746570944E736261756268188C0A4D6F6E74684669656C649493942981947D9428681D8C056D6F6E746894681F8868205D9468242981947D9468274E736261756268188C0F4461794F664D6F6E74684669656C649493942981947D9428681D8C0364617994681F8868205D9468242981947D9468274E736261756268188C095765656B4669656C649493942981947D9428681D8C047765656B94681F8868205D9468242981947D9468274E736261756268188C0E4461794F665765656B4669656C649493942981947D9428681D8C0B6461795F6F665F7765656B94681F8868205D9468242981947D9468274E7362617562681A2981947D9428681D8C04686F757294681F8868205D9468242981947D9468274E7362617562681A2981947D9428681D8C066D696E75746594681F8868205D9468242981947D9468274E7362617562681A2981947D9428681D8C067365636F6E6494681F8968205D9468228C0F52616E676545787072657373696F6E9493942981947D942868274E8C056669727374944B058C046C617374944B057562617562658C066A6974746572944E75628C086578656375746F72948C0764656661756C74948C046172677394298C066B7761726773947D94681D8C08746573745F6A6F62948C126D6973666972655F67726163655F74696D65944B018C08636F616C6573636594888C0D6D61785F696E7374616E636573944B018C0D6E6578745F72756E5F74696D65948C086461746574696D65948C086461746574696D65949394430A07E20A1E0D370500000094680F2868104D80704B008C03435354947494529486945294752E);
INSERT INTO `django_apscheduler_djangojob` VALUES ('2', '41694213b14741209e3d09f3d0ee803b', '2018-10-30 05:54:43', 0x800495F7010000000000007D94288C0776657273696F6E944B018C026964948C203431363934323133623134373431323039653364303966336430656538303362948C0466756E63948C18747261696E557365722E76696577733A746573745F6A6F62948C0774726967676572948C1D61707363686564756C65722E74726967676572732E696E74657276616C948C0F496E74657276616C547269676765729493942981947D942868014B028C0874696D657A6F6E65948C047079747A948C025F70949394288C0D417369612F5368616E67686169944DE8714B008C034C4D5494749452948C0A73746172745F64617465948C086461746574696D65948C086461746574696D65949394430A07E20A1901202B0C582894680F2868104D80704B008C034353549474945294869452948C08656E645F64617465944E8C08696E74657276616C9468158C0974696D6564656C74619493944B004B034B00879452948C066A6974746572944E75628C086578656375746F72948C0764656661756C74948C046172677394298C066B7761726773947D948C046E616D65948C08746573745F6A6F62948C126D6973666972655F67726163655F74696D65944B018C08636F616C6573636594888C0D6D61785F696E7374616E636573944B018C0D6E6578745F72756E5F74696D65946817430A07E20A1E0D362B0C582894681B86945294752E);
INSERT INTO `django_apscheduler_djangojob` VALUES ('3', '1a8ea3b495af4dc7b808e9b81d4cdf5d', '2018-10-30 05:54:41', 0x800495F7010000000000007D94288C0776657273696F6E944B018C026964948C203161386561336234393561663464633762383038653962383164346364663564948C0466756E63948C18747261696E557365722E76696577733A746573745F6A6F62948C0774726967676572948C1D61707363686564756C65722E74726967676572732E696E74657276616C948C0F496E74657276616C547269676765729493942981947D942868014B028C0874696D657A6F6E65948C047079747A948C025F70949394288C0D417369612F5368616E67686169944DE8714B008C034C4D5494749452948C0A73746172745F64617465948C086461746574696D65948C086461746574696D65949394430A07E20A190122350B258494680F2868104D80704B008C034353549474945294869452948C08656E645F64617465944E8C08696E74657276616C9468158C0974696D6564656C74619493944B004B034B00879452948C066A6974746572944E75628C086578656375746F72948C0764656661756C74948C046172677394298C066B7761726773947D948C046E616D65948C08746573745F6A6F62948C126D6973666972655F67726163655F74696D65944B018C08636F616C6573636594888C0D6D61785F696E7374616E636573944B018C0D6E6578745F72756E5F74696D65946817430A07E20A1E0D36290B258494681B86945294752E);
INSERT INTO `django_apscheduler_djangojob` VALUES ('4', 'cf9537e85fa24d16aa148006fa178020', '2018-10-30 05:54:50', 0x800495F7010000000000007D94288C0776657273696F6E944B018C026964948C206366393533376538356661323464313661613134383030366661313738303230948C0466756E63948C18747261696E557365722E76696577733A746573745F6A6F62948C0774726967676572948C1D61707363686564756C65722E74726967676572732E696E74657276616C948C0F496E74657276616C547269676765729493942981947D942868014B028C0874696D657A6F6E65948C047079747A948C025F70949394288C0D417369612F5368616E67686169944DE8714B008C034C4D5494749452948C0A73746172745F64617465948C086461746574696D65948C086461746574696D65949394430A07E20A190A2332061A8094680F2868104D80704B008C034353549474945294869452948C08656E645F64617465944E8C08696E74657276616C9468158C0974696D6564656C74619493944B004B3C4B00879452948C066A6974746572944E75628C086578656375746F72948C0764656661756C74948C046172677394298C066B7761726773947D948C046E616D65948C08746573745F6A6F62948C126D6973666972655F67726163655F74696D65944B018C08636F616C6573636594888C0D6D61785F696E7374616E636573944B018C0D6E6578745F72756E5F74696D65946817430A07E20A1E0D3632061A8094681B86945294752E);
INSERT INTO `django_apscheduler_djangojob` VALUES ('5', '511ba746b7554284b874d96657696660', '2018-10-30 05:54:46', 0x800495F7010000000000007D94288C0776657273696F6E944B018C026964948C203531316261373436623735353432383462383734643936363537363936363630948C0466756E63948C18747261696E557365722E76696577733A746573745F6A6F62948C0774726967676572948C1D61707363686564756C65722E74726967676572732E696E74657276616C948C0F496E74657276616C547269676765729493942981947D942868014B028C0874696D657A6F6E65948C047079747A948C025F70949394288C0D417369612F5368616E67686169944DE8714B008C034C4D5494749452948C0A73746172745F64617465948C086461746574696D65948C086461746574696D65949394430A07E20A1E0D352E03E41894680F2868104D80704B008C034353549474945294869452948C08656E645F64617465944E8C08696E74657276616C9468158C0974696D6564656C74619493944B004B3C4B00879452948C066A6974746572944E75628C086578656375746F72948C0764656661756C74948C046172677394298C066B7761726773947D948C046E616D65948C08746573745F6A6F62948C126D6973666972655F67726163655F74696D65944B018C08636F616C6573636594888C0D6D61785F696E7374616E636573944B018C0D6E6578745F72756E5F74696D65946817430A07E20A1E0D362E03E41894681B86945294752E);

-- ----------------------------
-- Table structure for django_apscheduler_djangojobexecution
-- ----------------------------
DROP TABLE IF EXISTS `django_apscheduler_djangojobexecution`;
CREATE TABLE `django_apscheduler_djangojobexecution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `run_time` datetime NOT NULL,
  `duration` decimal(15,2) DEFAULT NULL,
  `started` decimal(15,2) DEFAULT NULL,
  `finished` decimal(15,2) DEFAULT NULL,
  `exception` varchar(1000) DEFAULT NULL,
  `traceback` longtext,
  `job_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_apscheduler_d_job_id_daf5090a_fk_django_ap` (`job_id`),
  KEY `django_apscheduler_djangojobexecution_run_time_16edd96b` (`run_time`),
  CONSTRAINT `django_apscheduler_d_job_id_daf5090a_fk_django_ap` FOREIGN KEY (`job_id`) REFERENCES `django_apscheduler_djangojob` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_apscheduler_djangojobexecution
-- ----------------------------
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('1', 'Executed', '2018-10-24 17:26:05', '2.01', '1540401967.05', '1540401969.06', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('2', 'Executed', '2018-10-24 17:27:05', '0.00', null, '1540402025.06', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('3', 'Executed', '2018-10-24 17:28:05', '-0.02', '1540402085.07', '1540402085.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('4', 'Executed', '2018-10-24 17:29:05', '-0.02', '1540402145.07', '1540402145.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('5', 'Executed', '2018-10-24 17:30:05', '-0.03', '1540402205.09', '1540402205.06', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('6', 'Executed', '2018-10-24 17:31:05', '-0.02', '1540402265.08', '1540402265.06', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('7', 'Executed', '2018-10-24 17:32:05', '2.00', '1540402363.88', '1540402365.88', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('8', 'Executed', '2018-10-24 17:32:43', '2.02', '1540402365.06', '1540402367.08', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('9', 'Executed', '2018-10-24 17:32:46', '-0.02', '1540402366.84', '1540402366.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('10', 'Executed', '2018-10-24 17:32:49', '2.00', '1540402369.83', '1540402371.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('11', 'Executed', '2018-10-24 17:32:52', '-0.02', '1540402372.84', '1540402372.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('12', 'Executed', '2018-10-24 17:32:55', '-0.02', '1540402375.84', '1540402375.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('13', 'Executed', '2018-10-24 17:32:58', '-0.02', '1540402378.84', '1540402378.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('14', 'Executed', '2018-10-24 17:33:01', '-0.02', '1540402381.84', '1540402381.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('15', 'Executed', '2018-10-24 17:34:49', '2.01', '1540402493.82', '1540402495.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('16', 'Executed', '2018-10-24 17:34:05', '2.00', '1540402493.83', '1540402495.83', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('17', 'Executed', '2018-10-24 17:34:52', '-0.03', '1540402494.12', '1540402494.09', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('18', 'Executed', '2018-10-24 17:34:53', '2.01', '1540402494.13', '1540402496.14', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('19', 'Executed', '2018-10-24 17:34:55', '-0.03', '1540402495.85', '1540402495.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('20', 'Executed', '2018-10-24 17:34:56', '-0.02', '1540402496.76', '1540402496.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('21', 'Executed', '2018-10-24 17:34:58', '-0.02', '1540402498.84', '1540402498.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('22', 'Executed', '2018-10-24 17:34:59', '-0.01', '1540402499.76', '1540402499.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('23', 'Executed', '2018-10-25 02:34:49', '-0.29', '1540434894.01', '1540434893.72', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('24', 'Executed', '2018-10-25 02:34:50', '-0.16', '1540434894.02', '1540434893.86', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('25', 'Executed', '2018-10-25 02:34:05', '-0.05', '1540434894.04', '1540434893.99', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('26', 'Executed', '2018-10-25 02:34:52', '-0.08', '1540434894.62', '1540434894.54', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('27', 'Executed', '2018-10-25 02:34:53', '-0.04', '1540434894.63', '1540434894.59', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('28', 'Executed', '2018-10-25 02:34:55', '-0.05', '1540434895.97', '1540434895.92', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('29', 'Executed', '2018-10-25 02:34:56', '-0.02', '1540434896.77', '1540434896.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('30', 'Executed', '2018-10-25 02:34:58', '-0.03', '1540434898.85', '1540434898.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('31', 'Executed', '2018-10-25 02:34:59', '-0.02', '1540434899.76', '1540434899.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('32', 'Executed', '2018-10-25 02:35:01', '-0.02', '1540434901.85', '1540434901.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('33', 'Executed', '2018-10-25 02:35:02', '2.00', '1540434902.75', '1540434904.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('34', 'Executed', '2018-10-25 02:35:04', '2.00', '1540434904.82', '1540434906.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('35', 'Executed', '2018-10-25 02:35:05', '2.09', '1540434905.06', '1540434907.15', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('36', 'Executed', '2018-10-25 02:35:05', '2.10', '1540434905.75', '1540434907.84', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('37', 'Executed', '2018-10-25 02:35:07', '2.02', '1540434907.83', '1540434909.85', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('38', 'Executed', '2018-10-25 02:35:08', '2.01', '1540434908.75', '1540434910.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('39', 'Executed', '2018-10-25 02:35:10', '1.99', '1540434910.84', '1540434912.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('40', 'Executed', '2018-10-25 02:35:11', '2.01', '1540434911.75', '1540434913.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('41', 'Executed', '2018-10-25 02:35:13', '2.04', '1540434913.83', '1540434915.86', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('42', 'Executed', '2018-10-25 02:35:14', '2.03', '1540434914.74', '1540434916.77', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('43', 'Executed', '2018-10-25 02:35:16', '0.00', null, '1540434916.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('44', 'Started execution', '2018-10-25 02:35:17', null, '1540434917.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('45', 'Executed', '2018-10-25 02:35:19', '0.00', null, '1540434919.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('46', 'Started execution', '2018-10-25 02:35:20', null, '1540434920.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('47', 'Executed', '2018-10-25 02:35:22', '0.00', null, '1540434922.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('48', 'Executed', '2018-10-25 02:35:23', '0.00', null, '1540434923.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('49', 'Executed', '2018-10-25 02:35:25', '0.00', null, '1540434925.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('50', 'Executed', '2018-10-25 02:35:26', '0.00', null, '1540434926.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('51', 'Executed', '2018-10-25 02:35:28', '0.00', null, '1540434928.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('52', 'Started execution', '2018-10-25 02:35:29', null, '1540434929.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('53', 'Executed', '2018-10-25 02:35:31', '0.00', null, '1540434931.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('54', 'Executed', '2018-10-25 02:35:32', '0.00', null, '1540434932.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('55', 'Executed', '2018-10-25 02:35:34', '0.00', null, '1540434934.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('56', 'Executed', '2018-10-25 02:35:35', '0.00', null, '1540434935.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('57', 'Executed', '2018-10-25 02:35:37', '0.00', null, '1540434937.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('58', 'Executed', '2018-10-25 02:35:38', '0.00', null, '1540434938.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('59', 'Executed', '2018-10-25 02:35:40', '0.00', null, '1540434940.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('60', 'Executed', '2018-10-25 02:35:41', '0.00', null, '1540434941.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('61', 'Started execution', '2018-10-25 02:35:43', null, '1540434943.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('62', 'Started execution', '2018-10-25 02:35:44', null, '1540434944.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('63', 'Started execution', '2018-10-25 02:35:46', null, '1540434946.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('64', 'Executed', '2018-10-25 02:35:47', '0.00', null, '1540434947.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('65', 'Executed', '2018-10-25 02:35:49', '0.00', null, '1540434949.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('66', 'Executed', '2018-10-25 02:35:50', '0.00', null, '1540434950.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('67', 'Executed', '2018-10-25 02:35:50', '0.00', null, '1540434950.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('68', 'Started execution', '2018-10-25 02:35:52', null, '1540434952.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('69', 'Started execution', '2018-10-25 02:35:53', null, '1540434953.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('70', 'Started execution', '2018-10-25 02:35:55', null, '1540434955.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('71', 'Executed', '2018-10-25 02:35:56', '0.00', null, '1540434956.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('72', 'Started execution', '2018-10-25 02:35:58', null, '1540434958.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('73', 'Started execution', '2018-10-25 02:35:59', null, '1540434959.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('74', 'Started execution', '2018-10-25 02:36:01', null, '1540434961.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('75', 'Started execution', '2018-10-25 02:36:02', null, '1540434962.74', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('76', 'Started execution', '2018-10-25 02:36:04', null, '1540434964.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('77', 'Executed', '2018-10-25 02:36:05', '0.00', null, '1540434965.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('78', 'Executed', '2018-10-25 02:36:05', '0.00', null, '1540434965.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('79', 'Executed', '2018-10-25 02:36:07', '0.00', null, '1540434967.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('80', 'Executed', '2018-10-25 02:36:08', '0.00', null, '1540434968.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('81', 'Executed', '2018-10-25 02:36:10', '0.00', null, '1540434970.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('82', 'Executed', '2018-10-25 02:36:11', '0.00', null, '1540434971.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('83', 'Executed', '2018-10-25 02:36:13', '0.00', null, '1540434973.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('84', 'Started execution', '2018-10-25 02:36:14', null, '1540434974.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('85', 'Executed', '2018-10-25 02:36:16', '0.00', null, '1540434976.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('86', 'Started execution', '2018-10-25 02:36:17', null, '1540434977.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('87', 'Executed', '2018-10-25 02:36:19', '0.00', null, '1540434979.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('88', 'Executed', '2018-10-25 02:36:20', '0.00', null, '1540434980.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('89', 'Started execution', '2018-10-25 02:36:22', null, '1540434982.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('90', 'Executed', '2018-10-25 02:36:23', '0.00', null, '1540434983.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('91', 'Executed', '2018-10-25 02:36:25', '0.00', null, '1540434985.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('92', 'Executed', '2018-10-25 02:36:26', '0.00', null, '1540434986.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('93', 'Started execution', '2018-10-25 02:36:28', null, '1540434988.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('94', 'Executed', '2018-10-25 02:36:29', '0.00', null, '1540434989.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('95', 'Executed', '2018-10-25 02:36:31', '0.00', null, '1540434991.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('96', 'Started execution', '2018-10-25 02:36:32', null, '1540434992.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('97', 'Started execution', '2018-10-25 02:36:34', null, '1540434994.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('98', 'Started execution', '2018-10-25 02:36:35', null, '1540434995.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('99', 'Executed', '2018-10-25 02:36:37', '0.00', null, '1540434997.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('100', 'Executed', '2018-10-25 02:36:38', '0.00', null, '1540434998.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('101', 'Executed', '2018-10-25 02:36:40', '0.00', null, '1540435000.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('102', 'Executed', '2018-10-25 02:36:41', '0.00', null, '1540435001.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('103', 'Executed', '2018-10-25 02:36:43', '0.00', null, '1540435003.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('104', 'Started execution', '2018-10-25 02:36:44', null, '1540435004.74', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('105', 'Started execution', '2018-10-25 02:36:46', null, '1540435006.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('106', 'Executed', '2018-10-25 02:36:47', '0.00', null, '1540435007.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('107', 'Executed', '2018-10-25 02:36:49', '0.00', null, '1540435009.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('108', 'Executed', '2018-10-25 02:36:50', '0.00', null, '1540435010.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('109', 'Executed', '2018-10-25 02:36:50', '0.00', null, '1540435010.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('110', 'Started execution', '2018-10-25 02:36:52', null, '1540435012.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('111', 'Executed', '2018-10-25 02:36:53', '0.00', null, '1540435013.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('112', 'Started execution', '2018-10-25 02:36:55', null, '1540435015.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('113', 'Started execution', '2018-10-25 02:36:56', null, '1540435016.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('114', 'Executed', '2018-10-25 02:36:58', '0.00', null, '1540435018.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('115', 'Executed', '2018-10-25 02:36:59', '0.00', null, '1540435019.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('116', 'Executed', '2018-10-25 02:37:01', '0.00', null, '1540435021.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('117', 'Executed', '2018-10-25 02:37:02', '0.00', null, '1540435022.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('118', 'Executed', '2018-10-25 02:37:04', '0.00', null, '1540435024.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('119', 'Executed', '2018-10-25 02:37:05', '0.00', null, '1540435025.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('120', 'Executed', '2018-10-25 02:37:05', '0.00', null, '1540435025.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('121', 'Executed', '2018-10-25 02:37:07', '0.00', null, '1540435027.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('122', 'Started execution', '2018-10-25 02:37:08', null, '1540435028.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('123', 'Executed', '2018-10-25 02:37:10', '0.00', null, '1540435030.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('124', 'Executed', '2018-10-25 02:37:11', '0.00', null, '1540435031.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('125', 'Executed', '2018-10-25 02:37:13', '0.00', null, '1540435033.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('126', 'Executed', '2018-10-25 02:37:14', '0.00', null, '1540435034.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('127', 'Started execution', '2018-10-25 02:37:16', null, '1540435036.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('128', 'Started execution', '2018-10-25 02:37:17', null, '1540435037.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('129', 'Executed', '2018-10-25 02:37:19', '0.00', null, '1540435039.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('130', 'Executed', '2018-10-25 02:37:20', '0.00', null, '1540435040.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('131', 'Executed', '2018-10-25 02:37:22', '0.00', null, '1540435042.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('132', 'Executed', '2018-10-25 02:37:23', '0.00', null, '1540435043.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('133', 'Started execution', '2018-10-25 02:37:25', null, '1540435045.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('134', 'Executed', '2018-10-25 02:37:26', '0.00', null, '1540435046.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('135', 'Executed', '2018-10-25 02:37:28', '0.00', null, '1540435048.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('136', 'Executed', '2018-10-25 02:37:29', '0.00', null, '1540435049.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('137', 'Started execution', '2018-10-25 02:37:31', null, '1540435051.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('138', 'Executed', '2018-10-25 02:37:32', '0.00', null, '1540435052.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('139', 'Executed', '2018-10-25 02:37:34', '0.00', null, '1540435054.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('140', 'Started execution', '2018-10-25 02:37:35', null, '1540435055.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('141', 'Started execution', '2018-10-25 02:37:37', null, '1540435057.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('142', 'Started execution', '2018-10-25 02:37:38', null, '1540435058.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('143', 'Executed', '2018-10-25 02:37:40', '0.00', null, '1540435060.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('144', 'Executed', '2018-10-25 02:37:41', '0.00', null, '1540435061.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('145', 'Executed', '2018-10-25 02:37:43', '0.00', null, '1540435063.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('146', 'Started execution', '2018-10-25 02:37:44', null, '1540435064.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('147', 'Started execution', '2018-10-25 02:37:46', null, '1540435066.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('148', 'Executed', '2018-10-25 02:37:47', '0.00', null, '1540435067.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('149', 'Executed', '2018-10-25 02:37:49', '0.00', null, '1540435069.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('150', 'Executed', '2018-10-25 02:37:50', '0.00', null, '1540435070.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('151', 'Started execution', '2018-10-25 02:37:50', null, '1540435070.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('152', 'Started execution', '2018-10-25 02:37:52', null, '1540435072.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('153', 'Executed', '2018-10-25 02:37:53', '0.00', null, '1540435073.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('154', 'Started execution', '2018-10-25 02:37:55', null, '1540435075.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('155', 'Executed', '2018-10-25 02:37:56', '0.00', null, '1540435076.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('156', 'Executed', '2018-10-25 02:37:58', '0.00', null, '1540435078.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('157', 'Executed', '2018-10-25 02:37:59', '0.00', null, '1540435079.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('158', 'Executed', '2018-10-25 02:38:01', '0.00', null, '1540435081.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('159', 'Executed', '2018-10-25 02:38:02', '0.00', null, '1540435082.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('160', 'Executed', '2018-10-25 02:38:04', '0.00', null, '1540435084.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('161', 'Executed', '2018-10-25 02:38:05', '0.00', null, '1540435085.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('162', 'Executed', '2018-10-25 02:38:05', '0.00', null, '1540435085.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('163', 'Executed', '2018-10-25 02:38:07', '0.00', null, '1540435087.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('164', 'Executed', '2018-10-25 02:38:08', '0.00', null, '1540435088.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('165', 'Started execution', '2018-10-25 02:38:10', null, '1540435090.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('166', 'Started execution', '2018-10-25 02:38:11', null, '1540435091.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('167', 'Started execution', '2018-10-25 02:38:13', null, '1540435093.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('168', 'Executed', '2018-10-25 02:38:14', '0.00', null, '1540435094.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('169', 'Executed', '2018-10-25 02:38:16', '0.00', null, '1540435096.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('170', 'Executed', '2018-10-25 02:38:17', '0.00', null, '1540435097.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('171', 'Started execution', '2018-10-25 02:38:19', null, '1540435099.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('172', 'Started execution', '2018-10-25 02:38:20', null, '1540435100.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('173', 'Executed', '2018-10-25 02:38:22', '0.00', null, '1540435102.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('174', 'Started execution', '2018-10-25 02:38:23', null, '1540435103.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('175', 'Executed', '2018-10-25 02:38:25', '0.00', null, '1540435105.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('176', 'Executed', '2018-10-25 02:38:26', '0.00', null, '1540435106.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('177', 'Started execution', '2018-10-25 02:38:28', null, '1540435108.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('178', 'Executed', '2018-10-25 02:38:29', '0.00', null, '1540435109.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('179', 'Executed', '2018-10-25 02:38:31', '0.00', null, '1540435111.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('180', 'Executed', '2018-10-25 02:38:32', '0.00', null, '1540435112.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('181', 'Executed', '2018-10-25 02:38:34', '0.00', null, '1540435114.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('182', 'Started execution', '2018-10-25 02:38:35', null, '1540435115.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('183', 'Executed', '2018-10-25 02:38:37', '0.00', null, '1540435117.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('184', 'Started execution', '2018-10-25 02:38:38', null, '1540435118.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('185', 'Executed', '2018-10-25 02:38:40', '0.00', null, '1540435120.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('186', 'Executed', '2018-10-25 02:38:41', '0.00', null, '1540435121.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('187', 'Executed', '2018-10-25 02:38:43', '0.00', null, '1540435123.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('188', 'Started execution', '2018-10-25 02:38:44', null, '1540435124.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('189', 'Executed', '2018-10-25 02:38:46', '0.00', null, '1540435126.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('190', 'Executed', '2018-10-25 02:38:47', '0.00', null, '1540435127.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('191', 'Executed', '2018-10-25 02:38:49', '0.00', null, '1540435129.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('192', 'Executed', '2018-10-25 02:38:50', '0.00', null, '1540435130.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('193', 'Executed', '2018-10-25 02:38:50', '0.00', null, '1540435130.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('194', 'Started execution', '2018-10-25 02:38:52', null, '1540435132.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('195', 'Executed', '2018-10-25 02:38:53', '0.00', null, '1540435133.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('196', 'Executed', '2018-10-25 02:38:55', '0.00', null, '1540435135.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('197', 'Started execution', '2018-10-25 02:38:56', null, '1540435136.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('198', 'Executed', '2018-10-25 02:38:58', '0.00', null, '1540435138.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('199', 'Executed', '2018-10-25 02:38:59', '0.00', null, '1540435139.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('200', 'Executed', '2018-10-25 02:39:01', '0.00', null, '1540435141.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('201', 'Started execution', '2018-10-25 02:39:02', null, '1540435142.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('202', 'Executed', '2018-10-25 02:39:04', '0.00', null, '1540435144.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('203', 'Executed', '2018-10-25 02:39:05', '0.00', null, '1540435145.04', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('204', 'Executed', '2018-10-25 02:39:05', '0.00', null, '1540435145.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('205', 'Executed', '2018-10-25 02:39:07', '0.00', null, '1540435147.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('206', 'Started execution', '2018-10-25 02:39:08', null, '1540435148.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('207', 'Started execution', '2018-10-25 02:39:10', null, '1540435150.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('208', 'Started execution', '2018-10-25 02:39:11', null, '1540435151.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('209', 'Started execution', '2018-10-25 02:39:13', null, '1540435153.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('210', 'Executed', '2018-10-25 02:39:14', '0.00', null, '1540435154.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('211', 'Started execution', '2018-10-25 02:39:16', null, '1540435156.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('212', 'Executed', '2018-10-25 02:39:17', '0.00', null, '1540435157.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('213', 'Executed', '2018-10-25 02:39:19', '0.00', null, '1540435159.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('214', 'Started execution', '2018-10-25 02:39:20', null, '1540435160.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('215', 'Started execution', '2018-10-25 02:39:22', null, '1540435162.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('216', 'Executed', '2018-10-25 02:39:23', '0.00', null, '1540435163.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('217', 'Executed', '2018-10-25 02:39:25', '0.00', null, '1540435165.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('218', 'Executed', '2018-10-25 02:39:26', '0.00', null, '1540435166.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('219', 'Executed', '2018-10-25 02:39:28', '0.00', null, '1540435168.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('220', 'Executed', '2018-10-25 02:39:29', '0.00', null, '1540435169.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('221', 'Started execution', '2018-10-25 02:39:31', null, '1540435171.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('222', 'Executed', '2018-10-25 02:39:32', '0.00', null, '1540435172.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('223', 'Executed', '2018-10-25 02:39:34', '0.00', null, '1540435174.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('224', 'Executed', '2018-10-25 02:39:35', '0.00', null, '1540435175.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('225', 'Executed', '2018-10-25 02:39:37', '0.00', null, '1540435177.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('226', 'Executed', '2018-10-25 02:39:38', '0.00', null, '1540435178.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('227', 'Started execution', '2018-10-25 02:39:40', null, '1540435180.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('228', 'Executed', '2018-10-25 02:39:41', '0.00', null, '1540435181.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('229', 'Started execution', '2018-10-25 02:39:43', null, '1540435183.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('230', 'Executed', '2018-10-25 02:39:44', '0.00', null, '1540435184.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('231', 'Executed', '2018-10-25 02:39:46', '0.00', null, '1540435186.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('232', 'Started execution', '2018-10-25 02:39:47', null, '1540435187.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('233', 'Executed', '2018-10-25 02:39:49', '0.00', null, '1540435189.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('234', 'Started execution', '2018-10-25 02:39:50', null, '1540435190.42', null, null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('235', 'Started execution', '2018-10-25 02:39:50', null, '1540435190.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('236', 'Executed', '2018-10-25 02:39:52', '0.00', null, '1540435192.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('237', 'Started execution', '2018-10-25 02:39:53', null, '1540435193.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('238', 'Started execution', '2018-10-25 02:39:55', null, '1540435195.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('239', 'Started execution', '2018-10-25 02:39:56', null, '1540435196.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('240', 'Started execution', '2018-10-25 02:39:58', null, '1540435198.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('241', 'Started execution', '2018-10-25 02:39:59', null, '1540435199.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('242', 'Started execution', '2018-10-25 02:40:01', null, '1540435201.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('243', 'Executed', '2018-10-25 02:40:02', '0.00', null, '1540435202.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('244', 'Started execution', '2018-10-25 02:40:04', null, '1540435204.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('245', 'Executed', '2018-10-25 02:40:05', '0.00', null, '1540435205.07', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('246', 'Executed', '2018-10-25 02:40:05', '0.00', null, '1540435205.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('247', 'Started execution', '2018-10-25 02:40:07', null, '1540435207.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('248', 'Executed', '2018-10-25 02:40:08', '0.00', null, '1540435208.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('249', 'Executed', '2018-10-25 02:40:10', '0.00', null, '1540435210.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('250', 'Executed', '2018-10-25 02:40:11', '0.00', null, '1540435211.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('251', 'Executed', '2018-10-25 02:40:13', '0.00', null, '1540435213.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('252', 'Executed', '2018-10-25 02:40:14', '0.00', null, '1540435214.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('253', 'Started execution', '2018-10-25 02:40:16', null, '1540435216.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('254', 'Started execution', '2018-10-25 02:40:17', null, '1540435217.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('255', 'Executed', '2018-10-25 02:40:19', '0.00', null, '1540435219.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('256', 'Executed', '2018-10-25 02:40:20', '0.00', null, '1540435220.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('257', 'Executed', '2018-10-25 02:40:22', '0.00', null, '1540435222.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('258', 'Executed', '2018-10-25 02:40:23', '0.00', null, '1540435223.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('259', 'Executed', '2018-10-25 02:40:25', '0.00', null, '1540435225.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('260', 'Executed', '2018-10-25 02:40:26', '0.00', null, '1540435226.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('261', 'Executed', '2018-10-25 02:40:28', '0.00', null, '1540435228.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('262', 'Executed', '2018-10-25 02:40:29', '0.00', null, '1540435229.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('263', 'Executed', '2018-10-25 02:40:31', '0.00', null, '1540435231.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('264', 'Started execution', '2018-10-25 02:40:32', null, '1540435232.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('265', 'Executed', '2018-10-25 02:40:34', '0.00', null, '1540435234.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('266', 'Executed', '2018-10-25 02:40:35', '0.00', null, '1540435235.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('267', 'Executed', '2018-10-25 02:40:37', '0.00', null, '1540435237.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('268', 'Started execution', '2018-10-25 02:40:38', null, '1540435238.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('269', 'Executed', '2018-10-25 02:40:40', '0.00', null, '1540435240.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('270', 'Executed', '2018-10-25 02:40:41', '0.00', null, '1540435241.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('271', 'Executed', '2018-10-25 02:40:43', '0.00', null, '1540435243.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('272', 'Executed', '2018-10-25 02:40:44', '0.00', null, '1540435244.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('273', 'Executed', '2018-10-25 02:40:46', '0.00', null, '1540435246.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('274', 'Started execution', '2018-10-25 02:40:47', null, '1540435247.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('275', 'Started execution', '2018-10-25 02:40:49', null, '1540435249.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('276', 'Started execution', '2018-10-25 02:40:50', null, '1540435250.42', null, null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('277', 'Executed', '2018-10-25 02:40:50', '0.00', null, '1540435250.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('278', 'Executed', '2018-10-25 02:40:52', '0.00', null, '1540435252.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('279', 'Executed', '2018-10-25 02:40:53', '0.00', null, '1540435253.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('280', 'Executed', '2018-10-25 02:40:55', '0.00', null, '1540435255.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('281', 'Executed', '2018-10-25 02:40:56', '0.00', null, '1540435256.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('282', 'Executed', '2018-10-25 02:40:58', '0.00', null, '1540435258.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('283', 'Started execution', '2018-10-25 02:40:59', null, '1540435259.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('284', 'Started execution', '2018-10-25 02:41:01', null, '1540435261.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('285', 'Executed', '2018-10-25 02:41:02', '0.00', null, '1540435262.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('286', 'Started execution', '2018-10-25 02:41:04', null, '1540435264.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('287', 'Executed', '2018-10-25 02:41:05', '0.00', null, '1540435265.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('288', 'Executed', '2018-10-25 02:41:05', '0.00', null, '1540435265.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('289', 'Started execution', '2018-10-25 02:41:07', null, '1540435267.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('290', 'Started execution', '2018-10-25 02:41:08', null, '1540435268.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('291', 'Executed', '2018-10-25 02:41:10', '0.00', null, '1540435270.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('292', 'Executed', '2018-10-25 02:41:11', '0.00', null, '1540435271.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('293', 'Executed', '2018-10-25 02:41:13', '0.00', null, '1540435273.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('294', 'Executed', '2018-10-25 02:41:14', '0.00', null, '1540435274.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('295', 'Executed', '2018-10-25 02:41:16', '0.00', null, '1540435276.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('296', 'Started execution', '2018-10-25 02:41:17', null, '1540435277.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('297', 'Started execution', '2018-10-25 02:41:19', null, '1540435279.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('298', 'Started execution', '2018-10-25 02:41:20', null, '1540435280.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('299', 'Started execution', '2018-10-25 02:41:22', null, '1540435282.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('300', 'Executed', '2018-10-25 02:41:23', '0.00', null, '1540435283.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('301', 'Started execution', '2018-10-25 02:41:25', null, '1540435285.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('302', 'Started execution', '2018-10-25 02:41:26', null, '1540435286.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('303', 'Executed', '2018-10-25 02:41:28', '0.00', null, '1540435288.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('304', 'Started execution', '2018-10-25 02:41:29', null, '1540435289.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('305', 'Started execution', '2018-10-25 02:41:31', null, '1540435291.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('306', 'Executed', '2018-10-25 02:41:32', '0.00', null, '1540435292.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('307', 'Started execution', '2018-10-25 02:41:34', null, '1540435294.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('308', 'Executed', '2018-10-25 02:41:35', '0.00', null, '1540435295.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('309', 'Started execution', '2018-10-25 02:41:37', null, '1540435297.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('310', 'Started execution', '2018-10-25 02:41:38', null, '1540435298.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('311', 'Executed', '2018-10-25 02:41:40', '0.00', null, '1540435300.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('312', 'Started execution', '2018-10-25 02:41:41', null, '1540435301.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('313', 'Executed', '2018-10-25 02:41:43', '0.00', null, '1540435303.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('314', 'Started execution', '2018-10-25 02:41:44', null, '1540435304.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('315', 'Started execution', '2018-10-25 02:41:46', null, '1540435306.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('316', 'Executed', '2018-10-25 02:41:47', '0.00', null, '1540435307.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('317', 'Executed', '2018-10-25 02:41:49', '0.00', null, '1540435309.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('318', 'Started execution', '2018-10-25 02:41:50', null, '1540435310.42', null, null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('319', 'Executed', '2018-10-25 02:41:50', '0.00', null, '1540435310.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('320', 'Executed', '2018-10-25 02:41:52', '0.00', null, '1540435312.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('321', 'Executed', '2018-10-25 02:41:53', '0.00', null, '1540435313.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('322', 'Executed', '2018-10-25 02:41:55', '0.00', null, '1540435315.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('323', 'Executed', '2018-10-25 02:41:56', '0.00', null, '1540435316.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('324', 'Executed', '2018-10-25 02:41:58', '0.00', null, '1540435318.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('325', 'Executed', '2018-10-25 02:41:59', '0.00', null, '1540435319.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('326', 'Executed', '2018-10-25 02:42:01', '0.00', null, '1540435321.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('327', 'Executed', '2018-10-25 02:42:02', '0.00', null, '1540435322.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('328', 'Started execution', '2018-10-25 02:42:04', null, '1540435324.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('329', 'Executed', '2018-10-25 02:42:05', '0.00', null, '1540435325.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('330', 'Executed', '2018-10-25 02:42:05', '0.00', null, '1540435325.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('331', 'Executed', '2018-10-25 02:42:07', '0.00', null, '1540435327.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('332', 'Executed', '2018-10-25 02:42:08', '0.00', null, '1540435328.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('333', 'Executed', '2018-10-25 02:42:10', '0.00', null, '1540435330.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('334', 'Started execution', '2018-10-25 02:42:11', null, '1540435331.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('335', 'Executed', '2018-10-25 02:42:13', '0.00', null, '1540435333.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('336', 'Executed', '2018-10-25 02:42:14', '0.00', null, '1540435334.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('337', 'Executed', '2018-10-25 02:42:16', '0.00', null, '1540435336.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('338', 'Executed', '2018-10-25 02:42:17', '0.00', null, '1540435337.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('339', 'Executed', '2018-10-25 02:42:19', '0.00', null, '1540435339.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('340', 'Started execution', '2018-10-25 02:42:20', null, '1540435340.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('341', 'Started execution', '2018-10-25 02:42:22', null, '1540435342.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('342', 'Started execution', '2018-10-25 02:42:23', null, '1540435343.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('343', 'Started execution', '2018-10-25 02:42:25', null, '1540435345.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('344', 'Started execution', '2018-10-25 02:42:26', null, '1540435346.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('345', 'Executed', '2018-10-25 02:42:28', '0.00', null, '1540435348.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('346', 'Executed', '2018-10-25 02:42:29', '0.00', null, '1540435349.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('347', 'Executed', '2018-10-25 02:42:31', '0.00', null, '1540435351.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('348', 'Started execution', '2018-10-25 02:42:32', null, '1540435352.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('349', 'Executed', '2018-10-25 02:42:34', '0.00', null, '1540435354.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('350', 'Started execution', '2018-10-25 02:42:35', null, '1540435355.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('351', 'Started execution', '2018-10-25 02:42:37', null, '1540435357.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('352', 'Executed', '2018-10-25 02:42:38', '0.00', null, '1540435358.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('353', 'Executed', '2018-10-25 02:42:40', '0.00', null, '1540435360.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('354', 'Started execution', '2018-10-25 02:42:41', null, '1540435361.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('355', 'Executed', '2018-10-25 02:42:43', '0.00', null, '1540435363.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('356', 'Started execution', '2018-10-25 02:42:44', null, '1540435364.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('357', 'Executed', '2018-10-25 02:42:46', '0.00', null, '1540435366.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('358', 'Executed', '2018-10-25 02:42:47', '0.00', null, '1540435367.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('359', 'Executed', '2018-10-25 02:42:49', '0.00', null, '1540435369.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('360', 'Executed', '2018-10-25 02:42:50', '0.00', null, '1540435370.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('361', 'Started execution', '2018-10-25 02:42:50', null, '1540435370.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('362', 'Executed', '2018-10-25 02:42:52', '0.00', null, '1540435372.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('363', 'Started execution', '2018-10-25 02:42:53', null, '1540435373.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('364', 'Started execution', '2018-10-25 02:42:55', null, '1540435375.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('365', 'Executed', '2018-10-25 02:42:56', '0.00', null, '1540435376.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('366', 'Started execution', '2018-10-25 02:42:58', null, '1540435378.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('367', 'Executed', '2018-10-25 02:42:59', '0.00', null, '1540435379.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('368', 'Started execution', '2018-10-25 02:43:01', null, '1540435381.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('369', 'Executed', '2018-10-25 02:43:02', '0.00', null, '1540435382.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('370', 'Executed', '2018-10-25 02:43:04', '0.00', null, '1540435384.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('371', 'Started execution', '2018-10-25 02:43:05', null, '1540435385.05', null, null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('372', 'Started execution', '2018-10-25 02:43:05', null, '1540435385.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('373', 'Executed', '2018-10-25 02:43:07', '0.00', null, '1540435387.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('374', 'Executed', '2018-10-25 02:43:08', '0.00', null, '1540435388.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('375', 'Executed', '2018-10-25 02:43:10', '0.00', null, '1540435390.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('376', 'Executed', '2018-10-25 02:43:11', '0.00', null, '1540435391.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('377', 'Started execution', '2018-10-25 02:43:13', null, '1540435393.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('378', 'Executed', '2018-10-25 02:43:14', '0.00', null, '1540435394.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('379', 'Executed', '2018-10-25 02:43:16', '0.00', null, '1540435396.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('380', 'Started execution', '2018-10-25 02:43:17', null, '1540435397.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('381', 'Started execution', '2018-10-25 02:43:19', null, '1540435399.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('382', 'Executed', '2018-10-25 02:43:20', '0.00', null, '1540435400.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('383', 'Started execution', '2018-10-25 02:43:22', null, '1540435402.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('384', 'Executed', '2018-10-25 02:43:23', '0.00', null, '1540435403.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('385', 'Started execution', '2018-10-25 02:43:25', null, '1540435405.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('386', 'Executed', '2018-10-25 02:43:26', '0.00', null, '1540435406.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('387', 'Started execution', '2018-10-25 02:43:28', null, '1540435408.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('388', 'Started execution', '2018-10-25 02:43:29', null, '1540435409.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('389', 'Executed', '2018-10-25 02:43:31', '0.00', null, '1540435411.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('390', 'Executed', '2018-10-25 02:43:32', '0.00', null, '1540435412.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('391', 'Executed', '2018-10-25 02:43:34', '0.00', null, '1540435414.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('392', 'Started execution', '2018-10-25 02:43:35', null, '1540435415.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('393', 'Executed', '2018-10-25 02:43:37', '0.00', null, '1540435417.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('394', 'Started execution', '2018-10-25 02:43:38', null, '1540435418.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('395', 'Executed', '2018-10-25 02:43:40', '0.00', null, '1540435420.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('396', 'Executed', '2018-10-25 02:43:41', '0.00', null, '1540435421.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('397', 'Executed', '2018-10-25 02:43:43', '0.00', null, '1540435423.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('398', 'Started execution', '2018-10-25 02:43:44', null, '1540435424.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('399', 'Started execution', '2018-10-25 02:43:46', null, '1540435426.82', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('400', 'Started execution', '2018-10-25 02:43:47', null, '1540435427.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('401', 'Executed', '2018-10-25 02:43:49', '0.00', null, '1540435429.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('402', 'Executed', '2018-10-25 02:43:50', '0.00', null, '1540435430.42', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('403', 'Executed', '2018-10-25 02:43:50', '0.00', null, '1540435430.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('404', 'Executed', '2018-10-25 02:43:52', '-0.01', '1540435432.86', '1540435432.85', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('405', 'Executed', '2018-10-25 02:43:53', '0.00', null, '1540435433.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('406', 'Started execution', '2018-10-25 02:43:55', null, '1540435435.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('407', 'Started execution', '2018-10-25 02:43:56', null, '1540435436.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('408', 'Executed', '2018-10-30 05:52:44', '-1.06', '1540878773.07', '1540878772.01', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('409', 'Executed', '2018-10-30 05:52:46', '-0.39', '1540878773.06', '1540878772.67', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('410', 'Executed', '2018-10-30 05:51:50', '-0.07', '1540878773.10', '1540878773.03', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('411', 'Executed', '2018-10-30 05:52:05', '1.87', '1540878773.09', '1540878774.96', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('412', 'Executed', '2018-10-30 05:52:50', '-0.12', '1540878773.24', '1540878773.12', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('413', 'Executed', '2018-10-30 05:52:52', '-0.09', '1540878773.25', '1540878773.16', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('414', 'Executed', '2018-10-30 05:52:50', '-0.06', '1540878773.26', '1540878773.20', null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('415', 'Executed', '2018-10-30 05:52:53', '-0.03', '1540878773.77', '1540878773.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('416', 'Executed', '2018-10-30 05:52:55', '-0.01', '1540878775.84', '1540878775.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('417', 'Executed', '2018-10-30 05:52:56', '-0.02', '1540878776.78', '1540878776.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('418', 'Executed', '2018-10-30 05:52:58', '2.04', '1540878778.82', '1540878780.86', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('419', 'Started execution', '2018-10-30 05:52:59', null, '1540878779.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('420', 'Executed', '2018-10-30 05:53:01', '2.03', '1540878781.82', '1540878783.85', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('421', 'Executed', '2018-10-30 05:53:02', '2.02', '1540878782.74', '1540878784.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('422', 'Executed', '2018-10-30 05:53:04', '2.02', '1540878784.82', '1540878786.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('423', 'Executed', '2018-10-30 05:53:05', '0.00', null, '1540878785.05', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('424', 'Executed', '2018-10-30 05:53:05', '2.10', '1540878785.75', '1540878787.85', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('425', 'Executed', '2018-10-30 05:53:07', '2.01', '1540878787.83', '1540878789.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('426', 'Executed', '2018-10-30 05:53:08', '2.01', '1540878788.75', '1540878790.76', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('427', 'Executed', '2018-10-30 05:53:10', '2.13', '1540878790.82', '1540878792.95', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('428', 'Executed', '2018-10-30 05:53:11', '0.00', null, '1540878791.77', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('429', 'Executed', '2018-10-30 05:53:13', '0.00', null, '1540878793.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('430', 'Executed', '2018-10-30 05:53:14', '0.00', null, '1540878794.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('431', 'Executed', '2018-10-30 05:53:16', '0.00', null, '1540878796.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('432', 'Started execution', '2018-10-30 05:53:17', null, '1540878797.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('433', 'Executed', '2018-10-30 05:53:19', '0.00', null, '1540878799.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('434', 'Executed', '2018-10-30 05:53:20', '0.00', null, '1540878800.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('435', 'Executed', '2018-10-30 05:53:22', '0.00', null, '1540878802.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('436', 'Executed', '2018-10-30 05:53:23', '0.00', null, '1540878803.77', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('437', 'Executed', '2018-10-30 05:53:25', '0.00', null, '1540878805.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('438', 'Executed', '2018-10-30 05:53:26', '0.00', null, '1540878806.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('439', 'Executed', '2018-10-30 05:53:28', '0.00', null, '1540878808.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('440', 'Executed', '2018-10-30 05:53:29', '0.00', null, '1540878809.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('441', 'Executed', '2018-10-30 05:53:31', '0.00', null, '1540878811.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('442', 'Started execution', '2018-10-30 05:53:32', null, '1540878812.75', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('443', 'Executed', '2018-10-30 05:53:34', '0.00', null, '1540878814.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('444', 'Executed', '2018-10-30 05:53:35', '0.00', null, '1540878815.74', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('445', 'Executed', '2018-10-30 05:53:37', '0.00', null, '1540878817.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('446', 'Executed', '2018-10-30 05:53:38', '0.00', null, '1540878818.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('447', 'Started execution', '2018-10-30 05:53:40', null, '1540878820.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('448', 'Executed', '2018-10-30 05:53:41', '0.00', null, '1540878821.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('449', 'Executed', '2018-10-30 05:53:43', '0.00', null, '1540878823.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('450', 'Executed', '2018-10-30 05:53:44', '0.00', null, '1540878824.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('451', 'Started execution', '2018-10-30 05:53:46', null, '1540878826.27', null, null, null, '5');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('452', 'Executed', '2018-10-30 05:53:46', '0.00', null, '1540878826.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('453', 'Executed', '2018-10-30 05:53:47', '0.00', null, '1540878827.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('454', 'Executed', '2018-10-30 05:53:49', '0.00', null, '1540878829.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('455', 'Started execution', '2018-10-30 05:53:50', null, '1540878830.42', null, null, null, '4');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('456', 'Executed', '2018-10-30 05:53:50', '0.00', null, '1540878830.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('457', 'Executed', '2018-10-30 05:53:52', '0.00', null, '1540878832.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('458', 'Executed', '2018-10-30 05:53:53', '0.00', null, '1540878833.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('459', 'Started execution', '2018-10-30 05:53:55', null, '1540878835.83', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('460', 'Executed', '2018-10-30 05:53:56', '0.00', null, '1540878836.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('461', 'Executed', '2018-10-30 05:53:58', '0.00', null, '1540878838.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('462', 'Executed', '2018-10-30 05:53:59', '0.00', null, '1540878839.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('463', 'Started execution', '2018-10-30 05:54:01', null, '1540878841.84', null, null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('464', 'Executed', '2018-10-30 05:54:02', '0.00', null, '1540878842.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('465', 'Executed', '2018-10-30 05:54:04', '0.00', null, '1540878844.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('466', 'Executed', '2018-10-30 05:54:05', '0.00', null, '1540878845.06', null, null, '1');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('467', 'Executed', '2018-10-30 05:54:05', '0.00', null, '1540878845.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('468', 'Executed', '2018-10-30 05:54:07', '0.00', null, '1540878847.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('469', 'Executed', '2018-10-30 05:54:08', '0.00', null, '1540878848.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('470', 'Executed', '2018-10-30 05:54:10', '0.00', null, '1540878850.84', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('471', 'Started execution', '2018-10-30 05:54:11', null, '1540878851.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('472', 'Executed', '2018-10-30 05:54:13', '0.00', null, '1540878853.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('473', 'Executed', '2018-10-30 05:54:14', '0.00', null, '1540878854.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('474', 'Executed', '2018-10-30 05:54:16', '0.00', null, '1540878856.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('475', 'Executed', '2018-10-30 05:54:17', '0.00', null, '1540878857.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('476', 'Executed', '2018-10-30 05:54:19', '0.00', null, '1540878859.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('477', 'Executed', '2018-10-30 05:54:20', '0.00', null, '1540878860.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('478', 'Executed', '2018-10-30 05:54:22', '0.00', null, '1540878862.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('479', 'Executed', '2018-10-30 05:54:23', '0.00', null, '1540878863.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('480', 'Executed', '2018-10-30 05:54:25', '0.00', null, '1540878865.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('481', 'Executed', '2018-10-30 05:54:26', '0.00', null, '1540878866.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('482', 'Executed', '2018-10-30 05:54:28', '0.00', null, '1540878868.82', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('483', 'Executed', '2018-10-30 05:54:29', '0.00', null, '1540878869.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('484', 'Executed', '2018-10-30 05:54:31', '0.00', null, '1540878871.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('485', 'Started execution', '2018-10-30 05:54:32', null, '1540878872.76', null, null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('486', 'Executed', '2018-10-30 05:54:34', '0.00', null, '1540878874.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('487', 'Executed', '2018-10-30 05:54:35', '0.00', null, '1540878875.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('488', 'Executed', '2018-10-30 05:54:37', '0.00', null, '1540878877.83', null, null, '2');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('489', 'Executed', '2018-10-30 05:54:38', '0.00', null, '1540878878.75', null, null, '3');
INSERT INTO `django_apscheduler_djangojobexecution` VALUES ('490', 'Executed', '2018-10-30 05:54:40', '0.00', null, '1540878880.83', null, null, '2');

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('4', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('11', 'django_apscheduler', 'djangojob');
INSERT INTO `django_content_type` VALUES ('12', 'django_apscheduler', 'djangojobexecution');
INSERT INTO `django_content_type` VALUES ('9', 'lesson', 'lesson');
INSERT INTO `django_content_type` VALUES ('13', 'lesson', 'signup');
INSERT INTO `django_content_type` VALUES ('7', 'paratest', 'user');
INSERT INTO `django_content_type` VALUES ('6', 'sessions', 'session');
INSERT INTO `django_content_type` VALUES ('8', 'trainUser', 'user');
INSERT INTO `django_content_type` VALUES ('10', 'trainUser', 'usertoken');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'contenttypes', '0001_initial', '2018-09-23 04:58:30');
INSERT INTO `django_migrations` VALUES ('2', 'auth', '0001_initial', '2018-09-23 04:58:31');
INSERT INTO `django_migrations` VALUES ('3', 'admin', '0001_initial', '2018-09-23 04:58:31');
INSERT INTO `django_migrations` VALUES ('4', 'admin', '0002_logentry_remove_auto_add', '2018-09-23 04:58:31');
INSERT INTO `django_migrations` VALUES ('5', 'contenttypes', '0002_remove_content_type_name', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('6', 'auth', '0002_alter_permission_name_max_length', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('7', 'auth', '0003_alter_user_email_max_length', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('8', 'auth', '0004_alter_user_username_opts', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('9', 'auth', '0005_alter_user_last_login_null', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('10', 'auth', '0006_require_contenttypes_0002', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('11', 'auth', '0007_alter_validators_add_error_messages', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('12', 'auth', '0008_alter_user_username_max_length', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('13', 'paratest', '0001_initial', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('14', 'sessions', '0001_initial', '2018-09-23 04:58:32');
INSERT INTO `django_migrations` VALUES ('15', 'trainUser', '0001_initial', '2018-09-25 02:34:25');
INSERT INTO `django_migrations` VALUES ('16', 'lesson', '0001_initial', '2018-09-25 03:13:30');
INSERT INTO `django_migrations` VALUES ('17', 'lesson', '0002_auto_20180925_1154', '2018-09-25 03:54:29');
INSERT INTO `django_migrations` VALUES ('18', 'auth', '0009_alter_user_last_name_max_length', '2018-10-23 07:43:52');
INSERT INTO `django_migrations` VALUES ('19', 'lesson', '0002_auto_20181023_1543', '2018-10-23 07:43:52');
INSERT INTO `django_migrations` VALUES ('20', 'trainUser', '0002_auto_20181023_1543', '2018-10-23 07:44:26');
INSERT INTO `django_migrations` VALUES ('21', 'trainUser', '0003_auto_20181023_1600', '2018-10-24 17:08:57');
INSERT INTO `django_migrations` VALUES ('22', 'django_apscheduler', '0001_initial', '2018-10-25 01:11:28');
INSERT INTO `django_migrations` VALUES ('23', 'django_apscheduler', '0002_auto_20180412_0758', '2018-10-25 01:11:28');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_session
-- ----------------------------

-- ----------------------------
-- Table structure for lesson_lesson
-- ----------------------------
DROP TABLE IF EXISTS `lesson_lesson`;
CREATE TABLE `lesson_lesson` (
  `lesson_id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `link_info` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `lesson_code` varchar(30) DEFAULT NULL,
  `lesson_status` varchar(1) DEFAULT NULL,
  `lesson_type` varchar(30) DEFAULT NULL,
  `realm_account` varchar(30) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `signIn_code` varchar(255) DEFAULT NULL,
  `publish_range` varchar(255) DEFAULT NULL,
  `score` varchar(10) DEFAULT NULL,
  `marks` varchar(10) DEFAULT NULL,
  `avgScore` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`lesson_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lesson_lesson
-- ----------------------------
INSERT INTO `lesson_lesson` VALUES ('1', '测试', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'guobx', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组,经纪业务测试三6', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('2', '测试1', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', ' shencong13424', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('3', '测试2', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('4', '测试3', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'guobx', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('5', '测试4', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'guobx', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('6', '测试5', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('7', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('8', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('9', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('10', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('11', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('12', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('13', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');
INSERT INTO `lesson_lesson` VALUES ('14', '测试6', '测试1212212112212', 'wwwwwwww', 'rrrrrrrrrrr', '6666', '1', '1', 'lili10723', '2018-10-15 21:37:14', '2018-10-31 21:37:17', '6666', '6767', '经纪业务测试三组1', '66', '666', '66');

-- ----------------------------
-- Table structure for lesson_signup
-- ----------------------------
DROP TABLE IF EXISTS `lesson_signup`;
CREATE TABLE `lesson_signup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_id` int(11) NOT NULL COMMENT '课程id',
  `realm_account` varchar(30) NOT NULL COMMENT '域账号',
  `sign_status` varchar(1) DEFAULT NULL COMMENT '1-已签到，0-未签到',
  `score` varchar(10) DEFAULT NULL COMMENT '评价得分',
  `evaluate_desc` varchar(1000) DEFAULT NULL COMMENT '评价描述',
  PRIMARY KEY (`id`,`lesson_id`,`realm_account`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lesson_signup
-- ----------------------------
INSERT INTO `lesson_signup` VALUES ('1', '1', 'guobx', '1', '10', '18');

-- ----------------------------
-- Table structure for trainuser_user
-- ----------------------------
DROP TABLE IF EXISTS `trainuser_user`;
CREATE TABLE `trainuser_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
  `empl_number` int(10) DEFAULT NULL COMMENT '员工号',
  `realm_account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '域账号',
  `password` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '部门',
  `test_group` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '小组',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '性别',
  `age` int(3) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trainuser_user
-- ----------------------------
INSERT INTO `trainuser_user` VALUES ('1', '郭碧仙', '3413', 'guobx', '111111', '经纪业务测试三组1', '经纪业务测试三组', 'guobx@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('2', '凌建英', '7541', 'lingjy07541', '111111', '经纪业务测试三2', 'PBOX测试组', 'lingjy07541@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('3', '李莉', '10723', 'lili10723', '111111', '经纪业务测试三组', 'PBOX测试组', 'lili10723@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('4', '谌聪', '13424', ' shencong13424', '111111', '经纪业务测试三6', 'PBOX测试组', 'shencong13424@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('5', '詹文胜', '14206', 'zhanws', '111111', '经纪业务测试三组', 'PBOX测试组', 'zhanws@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('6', '阮苏娜', '14352', 'ruansn', '111111', '经纪业务测试三组', 'IFS测试组', 'ruansn@hundsun.com', null, null);
INSERT INTO `trainuser_user` VALUES ('7', '王亚运', '16176', 'wangyy16176', '111111', '经纪业务测试三组', 'PBOX测试组', 'wangyy16176@hundsun.com', null, null);

-- ----------------------------
-- Table structure for trainuser_usertoken
-- ----------------------------
DROP TABLE IF EXISTS `trainuser_usertoken`;
CREATE TABLE `trainuser_usertoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `realm_account` varchar(30) NOT NULL,
  `user_token` varchar(300) NOT NULL,
  `login_day` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trainuser_usertoken
-- ----------------------------
INSERT INTO `trainuser_usertoken` VALUES ('3', '2', 'ZXlKMGVYQWlPaUpLVjFBaUxDSmhiR2NpT2lKa1pXWmhkV3gwSW4wOjFnRXMxRzpyY0JVSGdTaWpLMGNvZGhqTG92LVdMWnF3WFE.ZXlKeVpXRnNiVjloWTJOdmRXNTBJam9pTWlJc0ltbGhkQ0k2TVRVME1ESTRNekU1TUM0ME5EWjk6MWdFczFHOnB0UEFmejg1ejVIeU9keXI5dzBSaHJjTlNSaw.749cbbcd1433707d0ad3203a45c2826b', '2018-10-30 13:53:26');
INSERT INTO `trainuser_usertoken` VALUES ('4', 'fanxn19000', 'ZXlKMGVYQWlPaUpLVjFBaUxDSmhiR2NpT2lKa1pXWmhkV3gwSW4wOjFnRXM4djozamxuZjV1N3Q2MFk1Z2paNFpZSVc3YVk5a0E.ZXlKeVpXRnNiVjloWTJOdmRXNTBJam9pWm1GdWVHNHhPVEF3TUNJc0ltbGhkQ0k2TVRVME1ESTRNelkyTlM0NU16aDk6MWdFczh2Ok9HWngxM1VaTlhkUzUzb01HYUtldklsMU83MA.df488af19be400ddbb867f3a0108f100', '2018-10-23 16:34:25');
INSERT INTO `trainuser_usertoken` VALUES ('5', 'guobx', 'ZXlKMGVYQWlPaUpLVjFBaUxDSmhiR2NpT2lKa1pXWmhkV3gwSW4wOjFnSFVIVTptTERwMEZIcUdZUmFUUm9zc1NKX0h6NFZTekU.ZXlKeVpXRnNiVjloWTJOdmRXNTBJam9pWjNWdlluZ2lMQ0pwWVhRaU9qRTFOREE1TURZNU1qUXVPVEV6T1RrNU9IMDoxZ0hVSFU6UnZLSjZDdXAzeS1MaEtOX285MW1FS1otYy1J.1127e66926c32a414e3528f76d30c710', '2018-11-12 09:55:51');
