'''
author:fanxn19000
本方法用于处理Datetime格式转换json时的问题
'''
import json
import datetime
class DateEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,datetime.datetime):
           return obj.strftime('%Y-%m-%d %H:%M:%S')
        else:
            return json.JSONEncoder.default(self,obj)
