from django.core.mail import send_mail

#发送邮件
def sendEmail(emailSubject,emailMessage,fromEmail,toEmailList):
    send_mail(emailSubject,emailMessage,fromEmail,toEmailList, fail_silently=False)
