from django.db import models
class Lesson(models.Model):
    lesson_id = models.AutoField(primary_key=True)
    lesson_title = models.CharField(max_length=255)
    content = models.CharField(max_length=255)
    link_info = models.CharField(max_length=255)
    attachment = models.CharField(max_length=255)
    lesson_code = models.CharField(max_length=30)
    lesson_status = models.CharField(max_length=2)
    lesson_type = models.CharField(max_length=255)
    realm_account = models.CharField(max_length=30)
    start_time = models.DateTimeField(default=None)
    end_time = models.DateTimeField(default=None)
    address = models.CharField(default=None, max_length=255)
    signIn_code = models.CharField(max_length=10)
    publish_range = models.CharField(max_length=255)
    score = models.CharField(max_length=10)
    marks = models.CharField(max_length=10)
    avgScore = models.CharField(max_length=10)

    def __str__(self):
        return '%d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (self.lesson_id, self.lesson_title, self.content, self.link_info,
                                self.attachment,self.lesson_code,self.lesson_status,self.lesson_type,
                                self.realm_account,self.start_time,self.end_time,self.address,
                                self.signIn_code, self.publish_range, self.score, self.marks, self.avgScore)


class Signup(models.Model):
    id = models.AutoField(primary_key=True)
    lesson_id = models.IntegerField()
    realm_account = models.CharField(max_length=30)
    sign_status= models.CharField(max_length=1)
    score= models.CharField(max_length=10)
    evaluate_desc= models.CharField(max_length=1000)

    def __str__(self):
        return '%d %d %s %s %s %s' % (self.id,self.lesson_id,self.realm_account,self.sign_status
                                      , self.score, self.evaluate_desc)

class Course(models.Model):
    id = models.AutoField(primary_key=True)
    lesson_id = models.IntegerField()
    realm_account = models.CharField(max_length=30)
    path = models.CharField(max_length=255)

    def __str__(self):
        return '%d %d %s %s' % (self.id, self.lesson_id, self.realm_account, self.path)
