from django.conf.urls import  url
from .  import views

urlpatterns = [
    # 创建课程
    url(r'^create$', views.lessonCreate, name='lessonCreate'),
    # 课程信息修改
    url(r'^update$', views.lessonUpdate, name='lessonUpdate'),
    # 课程列表查询
    url(r'^qry$', views.lessonQry, name='lessonQry'),
    # 课程删除
    url(r'^delete$', views.lessonDelete, name='lessonDelete'),
    # 课程报名（该接口用于课程报名和取消报名）
    url(r'^signUp$', views.signUpLesson, name='signUpLesson'),
    # 课程发布（用于发布和取消发布）
    url(r'^publish$', views.publishLesson, name='publishLesson'),
    # 课程结束
    url(r'^finish$', views.finishedLesson, name='finishedLesson'),
    # 取消课程
    url(r'^cancele$', views.canceledLesson, name='canceledLesson'),
    # 设置课程分数
    url(r'^setScore$', views.setScoreOfLesson, name='setScoreOfLesson'),
    # 课程签到
    url(r'^signinLesson$', views.signInLesson, name='signInLesson'),
    # 获取自己编写的课程
    url(r'^selfLesson$', views.selfLesson, name='selfLesson'),
    # 查询自己已报名的课程
    url(r'^selfSignUpLesson$', views.selfSignUpLesson, name='selfSignUpLesson'),
    # 获取提示邮件的内容
    url(r'^emailDetail$', views.emailDetail, name='emailDetail'),
    # 上传附件
    url(r'^uploadAttachment', views.uploadAttachment, name='uploadAttachment'),
    # 详情查询
    url(r'^detail', views.lessonDetail, name='detail'),
    # 附件下载
    url(r'^course', views.course, name='course'),
    # 附件删除
    url(r'^deleteCourse', views.delete_course, name='deleteCourse'),
    #发送邮件
    url(r'^sendEmail', views.sendEmail, name='sendEmail'),
]
